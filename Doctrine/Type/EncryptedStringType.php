<?php


namespace Qinetix\DoctrineEncryptBundle\Doctrine\Type;

use Doctrine\DBAL\Types\TextType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class EncryptedStringType extends TextType
{
    const MYTYPE         = 'encryptedstring';
    protected $secretKey = 'abcdef';

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if( $value === null )
            return $value;

        $enc = \Qinetix\DoctrineEncryptBundle\QinetixDoctrineEncryptBundle::getEncryptor();
        return $enc->decrypt( $value );
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if( $value === null )
            return $value;

        $enc = \Qinetix\DoctrineEncryptBundle\QinetixDoctrineEncryptBundle::getEncryptor();
        return $enc->encrypt( $value );
    }

    public function getName()
    {
        return self::MYTYPE;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

}
