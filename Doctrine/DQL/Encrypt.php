<?php

namespace Qinetix\DoctrineEncryptBundle\Doctrine\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\InputParameter;
use Doctrine\ORM\Query\Lexer;

class Encrypt extends FunctionNode
{

    public $value = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->value = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {

        $enc = \Qinetix\DoctrineEncryptBundle\QinetixDoctrineEncryptBundle::getEncryptor();

        if( $this->value instanceof InputParameter ){
            $query     = $sqlWalker->getQuery( );
            $parameter = $query->getParameter( $this->value->name );
            $newvalue  = $enc->encrypt( $parameter->getValue() );
            $query->setParameter( $this->value->name , $newvalue );
        }

        return ":".$this->value;

    }

}
