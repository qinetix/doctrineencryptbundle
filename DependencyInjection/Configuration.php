<?php

namespace Qinetix\DoctrineEncryptBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('qinetix_doctrine_encrypt');

        $rootNode->children()
            ->scalarNode('encryptor_class')
                ->defaultValue('Qinetix\DoctrineEncryptBundle\Encryptor\AES256Encryptor')
            ->end()
            ->scalarNode('encryption_key')
                ->isRequired()
            ->end()
        ->end();

        return $treeBuilder;
    }
}
