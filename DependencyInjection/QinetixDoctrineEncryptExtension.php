<?php

namespace Qinetix\DoctrineEncryptBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class QinetixDoctrineEncryptExtension extends Extension implements PrependExtensionInterface
{

    public function prepend(ContainerBuilder $container)
    {

        $config = array(
            'orm' => array(
                'entity_managers' => array(
                    'default' => array(
                        'dql' => array(
                            'string_functions' => array(
                                'encryptedstring' => 'Qinetix\DoctrineEncryptBundle\Doctrine\DQL\Encrypt'
                            )
                        )
                    )
                )
            )
        );
        $container->prependExtensionConfig('doctrine', $config );

    }

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        //Set Container Parameters
        $container->setParameter("qinetix_doctrine_encrypt.encryptor_class", $config['encryptor_class'] );
        $container->setParameter("qinetix_doctrine_encrypt.encryption_key" , $config['encryption_key']  );


    }
}
