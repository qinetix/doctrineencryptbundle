<?php

namespace Qinetix\DoctrineEncryptBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Qinetix\DoctrineEncryptBundle\DependencyInjection\Compiler\ConfigurationPass;

class QinetixDoctrineEncryptBundle extends Bundle
{

    protected static $encryptor = null;

    public static function getEncryptor(){
        return self::$encryptor;
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ConfigurationPass());
    }

    public function boot()
    {

        //Register Doctrine Custom Data Type
        $em = $this->container->get("doctrine.orm.entity_manager");
        try
        {
            Type::addType("encryptedstring", "Qinetix\DoctrineEncryptBundle\Doctrine\Type\EncryptedStringType");

            $em->
                getConnection()->
                getDatabasePlatform()->
                registerDoctrineTypeMapping("encryptedstring", "encryptedstring");
        } catch (\Doctrine\DBAL\DBALException $e)
        {
            // For some reason this exception gets thrown during
            // the clearing of the cache. I didn't have time to
            // find out why :-)
        }

        //Save the current encryptor
        try{
            self::$encryptor = $this->container->get('qinetix_doctrine_encrypt.encryptor');
        }catch( \Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException $ex ){

        }

    }

}
