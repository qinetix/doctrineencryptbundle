<?php

namespace Qinetix\DoctrineEncryptBundle\Encryptor;

class AES256Encryptor implements EncryptorInterface {

    private $secretKey;

    public function __construct($key) {
        $this->secretKey = $key;
    }

    public function encrypt($data) {
        return trim(base64_encode(mcrypt_encrypt(
                                        MCRYPT_RIJNDAEL_256, $this->secretKey, $data, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND
                                        ))));
    }

    function decrypt($data) {
        return trim(mcrypt_decrypt(
                                MCRYPT_RIJNDAEL_256, $this->secretKey, base64_decode($data), MCRYPT_MODE_ECB, mcrypt_create_iv(
                                        mcrypt_get_iv_size(
                                                MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                        ), MCRYPT_RAND
                                )));
    }

}