<?php

namespace Qinetix\DoctrineEncryptBundle\Encryptor;

interface EncryptorInterface {


    public function __construct($secretKey);

    public function encrypt($data);

    public function decrypt($data);
}